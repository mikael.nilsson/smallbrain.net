﻿using System.Text.Json;

namespace smallbrain;

public class DNA
{
  public int inputLength { get; set; }
  public int outputLength { get; set; }

  public float[][] net { get; } //! Är det verkligen rimligt att ha med value fr brain här?

  public DNA()
  {
    this.inputLength = 2;
    this.outputLength = 2;

  }

  public DNA(int inputLength, int outputLength, float[][] net)
  {
    this.inputLength = inputLength;
    this.outputLength = outputLength;
    this.net = net;
  }

  public override string ToString()
  {
    return JsonSerializer.Serialize(this);
  }
}


public class Brain
{
  public float[][] net { get; }
  public int inputLength { get; }
  public int outputLength { get; }

  private Random rand;


  public void input(float[] input)
  {
    for (int i = 0; i < this.inputLength; i++)
    {
      this.net[i][0] = input[i];
    }
  }

  public float[] output
  {
    get
    {
      int outStart = this.net.GetLength(0) - this.outputLength;
      float[] result = new float[this.outputLength];

      for (int i = 0; i < this.outputLength; i++)
      {
        result[i] = this.net[outStart + i][0];
      }

      return result;
    }
  }

  public DNA GetDNA()
  {
    return new DNA(this.inputLength, this.outputLength, this.net);
  }

  public Brain()
  {
    this.inputLength = 2;
    this.outputLength = 2;
    int netLength = 6;
    this.net = new float[6][];

    this.init(this.inputLength, this.outputLength, netLength);
  }

  public Brain(string DNA)
  {
    DNA? dna = JsonSerializer.Deserialize<DNA>(DNA);

    if (dna != null)
    {
      this.inputLength = dna.inputLength;
      this.outputLength = dna.outputLength;
      this.net = dna.net;
    }
    else
    {
      this.net = new float[6][];
      this.init(2, 2, 6);
    }

    this.rand = new Random((int)DateTime.Now.Ticks);
  }

  public Brain(int inputLength = 2, int outputLength = 2, int netLength = 0)
  {
    this.inputLength = inputLength;
    this.outputLength = outputLength;
    if (netLength == 0)
    {
      netLength = inputLength + outputLength + 2;
    }
    this.net = new float[netLength][];

    this.init(inputLength, outputLength, netLength);
  }

  public Brain(Brain p1, Brain p2)
  {
    this.rand = new Random((int)DateTime.Now.Ticks);

    // How many percent chance that each managed mutation happens
    // TODO: config
    int mutationRate = 50;

    int pChoice = rand.Next(2);

    this.inputLength = pChoice == 0 ? p1.inputLength : p2.inputLength;

    // mutate
    if (rand.Next(100) > mutationRate)
    {
      this.inputLength = rand.Next(2) > 1 ? this.inputLength + 1 : this.inputLength > 1 ? this.inputLength - 1 : 1;
    }

    this.outputLength = pChoice == 0 ? p1.outputLength : p2.outputLength;
    // mutate
    if (rand.Next(100) > mutationRate)
    {
      this.outputLength = rand.Next(2) > 1 ? this.outputLength + 1 : this.outputLength > 1 ? this.outputLength - 1 : 1;
    }

    // Simpler to set and change than to calculate and compare before setting
    float[][] shortNet = p1.net;
    float[][] longNet = p2.net;

    if (p1.net.GetLength(0) > p2.net.GetLength(0))
    {
      shortNet = p2.net;
      longNet = p1.net;
    }

    this.net = new float[longNet.Length][];

    // input
    createBrainInput();

    int shortLength = shortNet.GetLength(0);
    int longLength = longNet.GetLength(0);
    //  inheriting cells from rand parent for length of shortNet
    // ! We need to start att inputLength, and inherit the values from after inputLength of parents
    // ! The current approach might fail if length of parents differ much
    for (int i = this.inputLength; i < shortLength; i++)
    {
      float[] longCell = p1.net[i].GetLength(0) > p2.net[i].GetLength(0) ? p1.net[i] : p2.net[i];
      float[] shortCell = p1.net[i].GetLength(0) <= p2.net[i].GetLength(0) ? p1.net[i] : p2.net[i];

      float[] resultCell = new float[longCell.Length];

      for (int j = 1; j < shortCell.Length; j++)
      {
        resultCell[j] = rand.Next(2) == 1 ? longCell[j] : shortCell[j];
      }
      for (int j = shortCell.Length; j < longCell.Length; j++)
      {
        resultCell[j] = longCell[j];
      }

      this.net[i] = resultCell;
    }

  }

  private void createBrainInput()
  {
    for (int i = 0; i < this.inputLength; i++)
    {
      this.net[i] = new float[1];
      this.net[i][0] = 0;
    }
  }


  public Brain(int inputLength, int outputLength, float[][] net)
  {
    this.inputLength = inputLength;
    this.outputLength = outputLength;
    this.rand = new Random((int)DateTime.Now.Ticks);
    this.net = net;
  }


  /// <summary>
  /// Creates a net with attached sizes
  /// </summary>  
  private void init(int inputLength, int outputLength, int netLength)
  {
    this.rand = new Random((int)DateTime.Now.Ticks);

    createBrainInput();

    for (int i = this.inputLength; i < netLength; i++)
    {
      // [value, in0, weight0, op0, in1, weight1]

      int in0 = rand.Next(i);
      float w0 = (float)(rand.Next(20) - 10) / 10;
      int op0 = rand.Next(4);
      int in1 = rand.Next(i);
      float w1 = (float)(rand.Next(20) - 10) / 10;
      this.net[i] = new float[6] { 0, in0, w0, op0, in1, w1 };
    }
  }

  public void Iterate()
  {
    // +-*/
    // [val,src,wgt,op,src,wgt]
    // [[1],[2],[0,0,-2,0,0,-5],[0,2,-2,3,2,2],[0,3,4,1,2,-2],[0,2,-3,1,3,-4]]
    for (int i = this.inputLength; i < this.net.GetLength(0); i++)
    {
      float result = 0;
      for (int j = 1; j < this.net[i].Length; j++)
      {
        float op = 0;

        if (j > 1)
        {
          op = this.net[i][j];
          j++;
        }


        int source = Convert.ToInt32(this.net[i][j]);
        // Nåt är weird här så man kan behöva kontrollera att source inte är trasigt

        j++;
        float wgt = this.net[i][j];

        float sourceValue = this.net[source][0];

        string opVal = "";
        float oldResult = result;

        //+-*/
        switch (op)
        {
          case 0:
            opVal = "+";
            result = result + sourceValue * wgt;
            break;
          case 1:
            opVal = "-";
            result = result - sourceValue * wgt;
            break;
          case 2:
            opVal = "*";
            result = result * sourceValue * wgt;
            break;
          case 3:
            opVal = "/";
            if (result != 0 && sourceValue * wgt != 0)
            {
              result = result / sourceValue * wgt;
            }
            else result = 0;
            break;
        }

        // Console.WriteLine($"{result} = {oldResult} {opVal} {sourceValue}*{wgt}");

      }
      // Console.WriteLine(result);
      this.net[i][0] = result;
    }

  }

}
