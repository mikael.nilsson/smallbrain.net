
namespace smallbrain.test;

public class UnitTest1
{
  [Fact]
  public void TestIterate1()
  {
    Brain brain = new Brain();

    brain.Iterate();

    int outLength = brain.outputLength;
    Assert.NotNull(outLength);

    float[] output = brain.output;
    Assert.NotNull(output);
  }

  [Fact]
  public void TestBreedTwoBrains()
  {
    Brain b1 = new Brain();
    Brain b2 = new Brain();
    Brain child = new Brain(b1, b2);

    DNA dna = child.GetDNA();

    Console.WriteLine(b1.GetDNA().ToString());
    Console.WriteLine(b2.GetDNA().ToString());
    Console.WriteLine(child.GetDNA().ToString());

    Assert.True(dna.inputLength > 0);
    Assert.True(dna.outputLength > 0);
  }

  [Fact]
  public void TestMultipleGenerations()
  {
    // TODO: testa att skapa flera generationer och se att nätet utvecklas

    Brain[] brains = new Brain[10];

    for (int i = 0; i < 10; i++)
    {
      brains[i] = new Brain();
    }

    for (int i = 0; i < 10; i++)
    {
      brains[0] = new Brain(brains[8], brains[9]);
      for (int j = 1; j < 10; j++)
      {
        brains[j] = new Brain(brains[j - 1], brains[j]);
        Console.WriteLine(brains[j].GetDNA().ToString());
      }
    }

    Console.WriteLine();
  }

  [Fact]
  public void TestCreation()
  {
    Brain b = new Brain();

    Assert.NotNull(b.outputLength);
    DNA dna = b.GetDNA();

    Assert.Equal(2, dna.outputLength);
    Assert.Equal(dna.net.GetLength(0), 6);
  }

  [Fact]
  public void TestMultiCreation()
  {
    Brain b;

    for (int i = 0; i < 10; i++)
    {
      b = new Brain();
      DNA d = b.GetDNA();
      Console.WriteLine(d.ToString());
    }
  }

  [Fact(Skip = " ")]
  public void TestCreationFromDNAString()
  {
    Brain b1 = new Brain();

    string DNA = b1.GetDNA().ToString();

    Brain b2 = new Brain(DNA);

    Assert.Equal(b1.GetDNA().ToString(), b2.GetDNA().ToString());
  }

  [Fact]
  public void TestIteration()
  {
    // string DNA = $"{{\"inputLength\":2,\"outputLength\":2,\"net\":[[0],[0],[0,0,-2,0,0,-5],[0,2,-2,3,2,2],[0,3,4,1,2,-2],[0,2,-3,1,3,-4]]}}";
    float[][] net = new float[][] {
      new float[] {0},
      new float[] {0},
      new float[] {0,0,1,0,1,1}
    };

    Brain b = new Brain(1, 1, net);
    // Assert.Equal(d, b.GetDNA());

    // [value, in0, weight0, op0, in1, weight1]
    b.input(new float[] { 1 });
    // [[1],[2],[0,0,-2,0,0,-5],[0,2,-2,3,2,2],[0,3,4,1,2,-2],[0,2,-3,1,3,-4]]
    // [[1],[2],[-7,0,-2,0,0,-5],[-4,2,-2,3,2,2],[-30,3,4,1,2,-2],[5,2,-3,1,3,-4]]
    float[] expectedOutput = new float[] { 1 };

    b.Iterate();

    float[] output = b.output;
    Assert.Equal(expectedOutput, output);

  }

  [Fact(Skip = " ")]
  public void TestIteration2()
  {
    string DNA = $"{{\"inputLength\":3,\"outputLength\":3,\"net\":[[0],[0],[0],[0,0,-2,0,0,-1],[0,2,-1,3,2,2],[0,3,-1,1,2,-2],[0,2,-3,1,3,-4]]}}";

    Brain b = new Brain(DNA);
    Assert.Equal(DNA, b.GetDNA().ToString());

    // [value, in0, weight0, op0, in1, weight1]
    b.input(new float[] { 1, 2, 3 });
    // [[0],[0],[0],[0,0,-2,0,0,-1],[0,2,-1,3,2,2],[0,3,-1,1,2,-2],[0,2,-3,1,3,-4]]

    float[] expectedOutput = new float[] { -2, 9, -21 };

    b.Iterate();

    float[] output = b.output;
    Assert.Equal(expectedOutput, output);
  }


}

